package br.alura.refatoracao.cap4;

public abstract class ContaBancaria {

	protected String titular;
	protected double saldo;

	protected ContaBancaria(String titular, double saldoInicial) {
		this.titular = titular;
		this.saldo = saldoInicial;
	}
	
	protected void saca(double valor) {
		saldo -= valor;
	}
	
	protected void deposita(double valor) {
		saldo += valor;
	}
	
	protected double getSaldo() {
		return saldo;
	}
	
	protected String getTitular() {
		return titular;
	}
	
}
